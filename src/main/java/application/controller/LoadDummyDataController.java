package application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import application.repository.AuditoriumRepository;
import application.repository.MovieRepository;
import application.repository.SeatRepository;
import application.repository.TicketRepository;
import application.repository.UserRepository;
import application.repository.ViewingRepository;

@Controller
public class LoadDummyDataController {

    @Autowired
    AuditoriumRepository auditRepo;

    @Autowired
    MovieRepository movieRepo;

    @Autowired
    SeatRepository seatRepo;

    @Autowired
    TicketRepository ticketRepo;

    @Autowired
    UserRepository userRepo;

    @Autowired
    ViewingRepository viewRepo;

    @RequestMapping("/dummy")
    public void loadDummyData() {
        
//        List<Movie> movieList = IteratorUtils.toList(movieRepo.findAll().iterator());
//        
//        for (Movie movie : movieList) {
//            movie.setGenre("action");
//            movieRepo.save(movie);
//        }
        
//        char[] letters = new char[]{'A', 'B', 'C', 'D', 'E', 'F'};
//        
//        for (char c : letters) {
//            
//            for (int i = 0; i < 10; i++) {
//                Seat seat = new Seat();
//                seat.setAuditorium(4);
//                seat.setName(c + Integer.toString(i + 1));
//        
//                seatRepo.save(seat);
//            }
//            
//        }
                
        
                
    }

}
