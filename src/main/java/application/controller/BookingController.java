package application.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import application.database.model.Seat;
import application.database.model.Ticket;
import application.database.model.Viewing;
import application.repository.SeatRepository;
import application.repository.TicketRepository;
import application.repository.ViewingRepository;

@Controller
public class BookingController {

    @Autowired
    SeatRepository seatRepo;

    @Autowired
    ViewingRepository viewingRepo;
    
    @Autowired
    TicketRepository ticketRepo;

    private List<Seat> emptySeats;

    private Viewing viewing;

    @RequestMapping("/booking/{viewingId}")
    public ModelAndView showBookingPage(@PathVariable("viewingId") int viewingId) {
        ModelAndView modelAndView = new ModelAndView("booking");

        List<Seat> emptySeatsFromViewing = seatRepo.getEmptySeatsFromViewing(viewingId);

        viewing = viewingRepo.findOne(viewingId);
        emptySeats = emptySeatsFromViewing.stream().filter(s -> {
            return s.getAuditorium() == viewing.getAuditorium();
        }).collect(Collectors.toList());

        modelAndView.addObject("emptySeatCount", emptySeats.size());
        
        return modelAndView;
    }

    @RequestMapping("/confirmation")
    public ModelAndView confirmBooking(
            @RequestParam(value = "quantity", required = true) int ticketQuantity,
            @RequestParam(value = "cardNumber", required = true) String cardNumber,
            @RequestParam(value = "cvv", required = true) String cvv) {

        // Hash this
        String hash = cardNumber + cvv;
        
        System.out.println(emptySeats.size());
        for (int i = 0; i < ticketQuantity; i++) {
            Ticket ticket = new Ticket();
            ticket.setPin(hash);
            ticket.setViewing(viewing.getViewingId());
            
            Seat seat = emptySeats.get(0);
            emptySeats.remove(0);
            
            ticket.setSeat(seat.getSeatId());
            
            ticketRepo.save(ticket);
        }
        
        ModelAndView modelAndView = new ModelAndView("booking_confirmation");     
        
        return modelAndView;
    }
}
