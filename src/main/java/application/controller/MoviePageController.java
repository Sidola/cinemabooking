package application.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import application.database.model.Viewing;
import application.service.MovieService;
import application.service.SeatService;
import application.service.ViewingService;

@Controller
public class MoviePageController {

    // ----------------------------------------------
    //
    // Fields
    //
    // ----------------------------------------------

    @Autowired
    MovieService movieService;
    
    @Autowired
    ViewingService viewingService;
    
    @Autowired
    SeatService seatService;

    // ----------------------------------------------
    //
    // Mappings
    //
    // ----------------------------------------------

    @RequestMapping("/movie/{movieId}")
    public ModelAndView showMoviePage(@PathVariable("movieId") int movieId) {
        ModelAndView modelAndView = new ModelAndView("movie");
        
        modelAndView.addObject("pageTitle", "Movie Page Title");
        modelAndView.addObject("movie", movieService.getMovie(movieId));
        
        List<Viewing> viewings = viewingService.getViewings(movieId);
        modelAndView.addObject("viewings", viewings);
        
        Map<Integer, Integer> seatCountMap = new HashMap<>();
        for (Viewing viewing : viewings) {
            int viewingId = viewing.getViewingId();
            seatCountMap.put(viewingId, seatService.getEmptySeatCountForViewing(viewingId));
        }
        
        modelAndView.addObject("seatCountMap", seatCountMap);
        return modelAndView;
    }

    @RequestMapping("/getMovie")
    @ResponseBody
    public String getPlainText(@RequestParam("id") int id, HttpServletResponse response) {

        return "Ajax Test" + id;
    }

}
