package application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import application.service.MovieService;

@Controller
public class LandingPageController {

    // ----------------------------------------------
    //
    // Fields
    //
    // ----------------------------------------------

    @Autowired
    MovieService movieService;

    // ----------------------------------------------
    //
    // Mappings
    //
    // ----------------------------------------------

    @RequestMapping("/")
    public ModelAndView showLandingPage() {
        ModelAndView modelAndView = new ModelAndView("landing");

        modelAndView.addObject("pageTitle", "Landing Page Title");
        modelAndView.addObject("upcomingMoviesList", movieService.getUpcomingMovies());
        modelAndView.addObject("runningMoviesList", movieService.getRunningMovies());

        return modelAndView;
    }

}
