package application.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.database.model.Viewing;
import application.repository.ViewingRepository;
import application.service.ViewingService;

@Service
public class ViewingServiceImpl implements ViewingService {

    // ----------------------------------------------
    //
    // Fields
    //
    // ----------------------------------------------
    
    @Autowired
    ViewingRepository viewingRepository;
    
    // ----------------------------------------------
    //
    // Public API
    //
    // ----------------------------------------------
    
    @Override
    public List<Viewing> getViewings(int movieId) {
        return viewingRepository.getViewings(movieId);
    }

}
