package application.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.database.model.Auditorium;
import application.database.model.Seat;
import application.database.model.Ticket;
import application.database.model.Viewing;
import application.repository.AuditoriumRepository;
import application.repository.SeatRepository;
import application.repository.TicketRepository;
import application.repository.ViewingRepository;
import application.service.SeatService;

@Service
public class SeatServiceImpl implements SeatService {

    // ----------------------------------------------
    //
    // Fields
    //
    // ----------------------------------------------

    @Autowired
    private SeatRepository seatRepository;
    
    @Autowired
    private ViewingRepository viewingRepository; 
    
    @Autowired
    private TicketRepository ticketRepository;
    
    // ----------------------------------------------
    //
    // Public API
    //
    // ----------------------------------------------

    @Override
    public List<Seat> getSeatsForAuditorium(int auditoriumId) {
        return seatRepository.getSeatsForAuditorium(auditoriumId);
    }

    @Override
    public int getEmptySeatCountForViewing(int viewingId) {
        
        // Get the viewing
        Viewing viewing = viewingRepository.findOne(viewingId);        
        
        // Get all seats for that auditorium
        List<Seat> seatsForAuditorium = seatRepository.getSeatsForAuditorium(viewing.getAuditorium());
        
        // Get all tickets for this viewing
        List<Ticket> ticketsForViewing = ticketRepository.getTicketsForViewing(viewingId);
        
        // Subtract count from seatcount
        return seatsForAuditorium.size() - ticketsForViewing.size();
        
    }
    
    
    
}
