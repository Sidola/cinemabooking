package application.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.database.model.Movie;
import application.repository.MovieRepository;
import application.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService {

    // ----------------------------------------------
    //
    // Fields
    //
    // ----------------------------------------------

    @Autowired
    MovieRepository movieRepository;

    // ----------------------------------------------
    //
    // Public API
    //
    // ----------------------------------------------
    
    public List<Movie> getUpcomingMovies() {
        return movieRepository.getUpcomingMovies();
    }

    public List<Movie> getRunningMovies() {
        return movieRepository.getRunningMovies();
    }

    @Override
    public Movie getMovie(int movieId) {
        return movieRepository.getMovieById(movieId);
    }

}
