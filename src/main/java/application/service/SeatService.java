package application.service;

import java.util.List;

import org.springframework.stereotype.Service;

import application.database.model.Seat;

@Service
public interface SeatService {
    
    public List<Seat> getSeatsForAuditorium(int auditoriumId);
    
    public int getEmptySeatCountForViewing(int viewingId);
    
}
