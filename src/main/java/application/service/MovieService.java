package application.service;

import java.util.List;

import org.springframework.stereotype.Service;

import application.database.model.Movie;

@Service
public interface MovieService {

    public List<Movie> getUpcomingMovies();

    public List<Movie> getRunningMovies();
    
    public Movie getMovie(int movieId);

}
