package application.service;

import java.util.List;

import org.springframework.stereotype.Service;

import application.database.model.Viewing;

@Service
public interface ViewingService {

    public List<Viewing> getViewings(int movieId);
    
}
