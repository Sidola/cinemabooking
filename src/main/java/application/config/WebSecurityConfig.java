package application.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // @formatter:off
        
        http
            .authorizeRequests()
            
            // Permit access to resources
            .antMatchers("/**", "/css/**", "/js/**").permitAll()
            
            .antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
            .anyRequest().authenticated()
            .and()
            
            .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                
                    .logout()
                        .permitAll();

        // @formatter:on

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        // @formatter:off
        
        auth
            .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER");
        
        auth
            .inMemoryAuthentication()
                .withUser("admin").password("password").roles("ADMIN");
        
        // @formatter:on

    }

}
