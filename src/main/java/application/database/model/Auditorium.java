package application.database.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the auditorium database table.
 * 
 */
@Entity
@NamedQuery(name = "Auditorium.findAll", query = "SELECT a FROM Auditorium a")
public class Auditorium implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Auditorium_Id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int auditoriumId;

    private String address;

    private String description;

    private String name;

    public Auditorium() {
    }

    public int getAuditoriumId() {
        return this.auditoriumId;
    }

    public void setAuditoriumId(int auditoriumId) {
        this.auditoriumId = auditoriumId;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}