package application.database.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the ticket database table.
 * 
 */
@Entity
@NamedQuery(name = "Ticket.findAll", query = "SELECT t FROM Ticket t")
public class Ticket implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Ticket_Id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ticketId;

    private String pin;

    private int seat;

    private int viewing;

    public Ticket() {
    }

    public int getTicketId() {
        return this.ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getPin() {
        return this.pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getSeat() {
        return this.seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public int getViewing() {
        return this.viewing;
    }

    public void setViewing(int viewing) {
        this.viewing = viewing;
    }

}