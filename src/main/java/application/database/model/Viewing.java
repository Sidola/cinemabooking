package application.database.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

/**
 * The persistent class for the viewing database table.
 * 
 */
@Entity
@NamedQuery(name = "Viewing.findAll", query = "SELECT v FROM Viewing v")
public class Viewing implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Viewing_Id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int viewingId;

    private int auditorium;

    @Temporal(TemporalType.DATE)
    private Date date;

    private int movie;

    private Time time;

    public Viewing() {
    }

    public int getViewingId() {
        return this.viewingId;
    }

    public void setViewingId(int viewingId) {
        this.viewingId = viewingId;
    }

    public int getAuditorium() {
        return this.auditorium;
    }

    public void setAuditorium(int auditorium) {
        this.auditorium = auditorium;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMovie() {
        return this.movie;
    }

    public void setMovie(int movie) {
        this.movie = movie;
    }

    public Time getTime() {
        return this.time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

}