package application.database.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the movie database table.
 * 
 */
@Entity
@NamedQuery(name = "Movie.findAll", query = "SELECT m FROM Movie m")
public class Movie implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Movie_Id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int movieId;

    @Column(name = "Age_Limit")
    private int ageLimit;

    private String description;

    @Column(name = "Image_Id")
    private String imageId;

    private int runtime;

    private String status;

    private String title;

    private String genre;
    
    @Column(name = "Youtube_Trailer_Id")
    private String youtubeTrailerId;

    public Movie() {
    }

    public int getMovieId() {
        return this.movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getAgeLimit() {
        return this.ageLimit;
    }

    public void setAgeLimit(int ageLimit) {
        this.ageLimit = ageLimit;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageId() {
        return this.imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public int getRuntime() {
        return this.runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }
    
    public void setGenre(String genre) {
        this.genre = genre;
    }
    
    public String getYoutubeTrailerId() {
        return this.youtubeTrailerId;
    }

    public void setYoutubeTrailerId(String youtubeTrailerId) {
        this.youtubeTrailerId = youtubeTrailerId;
    }

}