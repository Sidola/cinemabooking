package application.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the seat database table.
 * 
 */
@Entity
@NamedQuery(name="Seat.findAll", query="SELECT s FROM Seat s")
public class Seat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Seat_Id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int seatId;

	private int auditorium;

	private String name;

	public Seat() {
	}

	public int getSeatId() {
		return this.seatId;
	}

	public void setSeatId(int seatId) {
		this.seatId = seatId;
	}

	public int getAuditorium() {
		return this.auditorium;
	}

	public void setAuditorium(int auditorium) {
		this.auditorium = auditorium;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}