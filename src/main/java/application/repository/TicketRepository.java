package application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import application.database.model.Ticket;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Integer> {
    
    @Query("select t from Ticket t where t.viewing = ?1")
    public List<Ticket> getTicketsForViewing(int viewingId);
    
}
