package application.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import application.database.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
}
