package application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import application.database.model.Viewing;

@Repository
public interface ViewingRepository extends CrudRepository<Viewing, Integer> {

    @Query("select v from Viewing v where v.movie = ?1")
    public List<Viewing> getViewings(int movieId);

}
