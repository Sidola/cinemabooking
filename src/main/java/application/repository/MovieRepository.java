package application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import application.database.model.Movie;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Integer> {

    @Query("select m from Movie m where m.status = 'upcoming'")
    public List<Movie> getUpcomingMovies();

    @Query("select m from Movie m where m.status = 'running'")
    public List<Movie> getRunningMovies();

    @Query("select m from Movie m where m.movieId = ?1")
    public Movie getMovieById(int id);
    
}
