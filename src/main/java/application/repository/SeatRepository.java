package application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import application.database.model.Seat;

@Repository
public interface SeatRepository extends CrudRepository<Seat, Integer> {

    @Query("select s from Seat s where s.auditorium = ?1")
    public List<Seat> getSeatsForAuditorium(int auditorium);

    @Query("Select s from Seat s where not exists"
            + "(select s from Ticket t where s.seatId = t.seat and t.viewing = ?1 )")
    public List<Seat> getEmptySeatsFromViewing(int viewing);
}
