package application.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import application.database.model.Auditorium;

@Repository
public interface AuditoriumRepository extends CrudRepository<Auditorium, Integer> {
}
