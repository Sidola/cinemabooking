
Extra notes:

	- 'movie' table needs a 'genre' column
	- 'movie' table needs an 'age_limit' column
	- 'ticket' table needs a 'card_number' column
		- Remove 'ssn' and 'pin' columns

	- 'genre' table needed
	- 'age_limit' table needed

	- 'admin' table, a list of users who are allowed to admin the site

-------------------------------------------------------------------------------------

Landing page:

	User requirements:

		- List of running movies
		- List of upcoming movies

		- Filter the lists by:
			- Running date (only on running)
			- Genre
			- Age requirement

	Backend requirements:

		- Query 'movie' table for all movies with the status 'running'
			- Use image for thumbnail
			- Generate an ID to navigate to this movie

		- Build up 'date' dropdown by taking todays date + 6 days in the future
			- Built when the page is requested

		- Query 'genre' table for all available genres
		- Query 'age_limit' table for all available age limit brackets

		- Query 'show' table with a date to get all movies running on that date
		- Query 'genre' table with a genre to get all movies of that genre
		- Query 'age_limit' table with an age_limit to get all movies for that age_limit

-------------------------------------------------------------------------------------

Selected movie:

	User requirements:

		- Movie info
			- Title
			- Description
			- Genre
			- Runtime

		- Dropdown with all dates this movie is running
		- List of showings for this movie (defaults to today, changed by selecting different date in the dropdown)
			- Each row has the show ID encoded for the booking step

		- Each showing has a button to book that particular show

	Backend requirements:

		- Query 'movie' for the movie with the correct ID
		- Query 'show' with the ID of this movie, and get all showings of this movie

-------------------------------------------------------------------------------------

Book showing:

	User requirements:

		- Select the amount of seats to book
		- Show empty seats and booked seats
		- Let the user visually select a seat among the empty seats
		- Let the user add a payment method

	Backend requirements:

		- Get all booked seats, display on website
		- Submit all the data from the user and create tickets
			- User will submit a show id several seat ids, together with a credit card number

-------------------------------------------------------------------------------------

Admin landing page:

	User requirements:

		- Login to management system

		- List of movies in the system
		- List of auditoriums in the system
		- List of shows

		- Buttons below each list to modify the selected entry in the list
		- Buttons below each list to add an entry to that list (movie, auditorium, show)
		- Buttons below each list to remove an entry

		- Filter movies by
			- Title
			- Status
			- Auditorium

		- Filter shows-list by
			- Date

		- Filter auditoriums by
			- Name
			- Running movies

	Backend requirements:

		- Query 'admin' table with login
		- Lock admin pages behind login

		- Query tables to fill lists

-------------------------------------------------------------------------------------

Movie admin page:

	User requirements:

		- Change/Add info about a movie
			- Name (Manual input)
			- Description (Manual input)
			- Runtime (Manual input)
			- Status (Dropdown)
			- Image thumbnail (Dropdown of found images (Image upload))
			- Youtube trailer (Manual input)

		- Buttons to save, cancel and delete entry

	Backend requirements:

		- Submit to database

-------------------------------------------------------------------------------------

Auditoriums admin page:

	User requirements:

		- Change/Add info about an auditorium
			- Name (Manual input)
			- Seats (Manual input)
			- Description (Manual input)
			- Address (Manual input)
			
		- Buttons to save, cancel and delete entry

	Backend requirements:

		- Submit to database

		- The seat count can only be specified for new entries, when modifying the seat input is grayed out
			- Generate entries in the 'seat' table based on the amount of seats supplied by the users, generate a name automatically using a letter and a digit, e.g. 'A1 -> A10', 'B1 -> B10'

-------------------------------------------------------------------------------------

Shows admin page:

	User requirements:

		- Change/Add info about a show
			- Movie (Dropdown from movie-table)
			- Auditorium (Dropdown from auditorium-table)
			- Date (Datepicker input)
			- Time (Timepicker input)

		- List of booked tickets for the show

		- Buttons to save, cancel

	Backend requirements:

		- Submit to database

		- A show cannot be edited after creation
		- A show cannot be deletedstisw11th
		