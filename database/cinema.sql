-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2016 at 07:23 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cinema`
--

-- --------------------------------------------------------

--
-- Table structure for table `auditorium`
--

CREATE TABLE IF NOT EXISTS `auditorium` (
`Auditorium_Id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Address` varchar(70) NOT NULL,
  `Description` varchar(120) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditorium`
--

INSERT INTO `auditorium` (`Auditorium_Id`, `Name`, `Address`, `Description`) VALUES
(1, 'Biosalong 1', 'Stadsgatan 3, 666 66 Staden', 'Handikappanpassad'),
(2, 'Biosalong 2', 'Stadsgatan 3, 666 66 Staden', NULL),
(3, 'Biosalong 3', 'Stadsgatan 3, 666 66 Staden', 'Hot stuff'),
(4, 'Biosalong 4', 'Stadsgatan 3, 666 66 Staden', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE IF NOT EXISTS `movie` (
`Movie_Id` int(11) NOT NULL,
  `Title` varchar(45) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `Status` enum('running','inactive','upcoming') NOT NULL,
  `Runtime` int(11) NOT NULL,
  `Youtube_Trailer_Id` varchar(100) NOT NULL,
  `Image_Id` varchar(45) NOT NULL,
  `Age_Limit` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`Movie_Id`, `Title`, `Description`, `Status`, `Runtime`, `Youtube_Trailer_Id`, `Image_Id`, `Age_Limit`) VALUES
(1, 'The Shawshank Redemption', 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.', 'running', 120, '6hB3S9bIaco', 'shawshank', 11),
(2, 'North by Northwest', 'A hapless New York advertising executive is mistaken for a government agent by a group of foreign spies, and is pursued across the country while he looks for a way to survive.', 'inactive', 136, 'qk0GbTMMbP0', 'North_by_Northwest', 11),
(3, 'The Godfather', 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.', 'running', 120, 'sY1S34973zA', 'the_god_father', 13),
(4, 'The Godfather: Part II', 'The early life and career of Vito Corleone in 1920s New York is portrayed while his son, Michael, expands and tightens his grip on his crime syndicate stretching from Lake Tahoe, Nevada to pre-revolution 1958 Cuba.', 'running', 120, 'qJr92K_hKl0', 'the_god_father_2', 13),
(5, 'The Dark Knight', 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the caped crusader must come to terms with one of the greatest psychological tests of his ability to fight injustice.', 'running', 120, 'EXeTwQWrcwY', 'the_dark_knight', 13),
(6, 'Pulp Fiction', 'The lives of two mob hit men, a boxer, a gangster''s wife, and a pair of diner bandits intertwine in four tales of violence and redemption.', 'running', 120, 's7EdQ4FqbhY', 'pulp_fiction', 13),
(7, 'Schindler''s List', '\r\nIn Poland during World War II, Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.', 'running', 120, 'JdRGC-w9syA', 'schindlers_list', 13),
(8, 'Yankee Doodle Dandy', 'A film of the life of the renowned musical composer, playwright, actor, dancer and singer George M. Cohan.', 'inactive', 126, 'qTec9v6fjN0', 'Yankee Doodle Dandy', 13),
(9, 'Rear Window', 'A rebellious young man with a troubled past comes to a new town, finding friends and enemies.', 'inactive', 111, 'wXRgAXU1-T4', 'Rear_Window', 13),
(10, '12 Angry Men', 'A dissenting juror in a murder trial slowly manages to convince the others that the case is not as obviously clear as it seemed in court.', 'running', 120, 'A7CBKT0PWFA', '12_angry_men', 13),
(11, 'Frankenstein', 'An obsessed scientist assembles a living being from parts of exhumed corpses.', 'upcoming', 70, 'bKyiXjyVsfw', 'Frankenstein', 13),
(12, 'Taxi Driver', 'A mentally unstable Vietnam war veteran works as a night-time taxi driver in New York City where the perceived decadence and sleaze feeds his urge for violent action, attempting to save a preadolescent prostitute in the process.', 'running', 113, 'cujiHDeqnHY', ' Taxi_Driver', 13),
(13, 'Pulp Fiction', 'The lives of two mob hit men, a boxer, a gangster''s wife, and a pair of diner bandits intertwine in.', 'running', 120, 'wZBfmBvvotE', ' Pulp_Fiction', 16),
(14, ' The Green Mile', 'The lives of guards on Death Row are affected by one of their charges: a black man accused of child murder and rape, yet who has a mysterious gift.', 'upcoming', 189, 'uDybmxbKf4Y', ' The_Green_Mile', 13),
(15, 'Rain Man', 'Selfish yuppie Charlie Babbitt''s father left a fortune to his savant brother Raymond and a pittance to Charlie; they travel cross-country.', 'running', 133, 'KKC3W0awjm0', 'Rain_Man', 13),
(16, 'City Lights', 'With the aid of a wealthy erratic tippler, a dewy-eyed tramp who has fallen in love with a sightless flower girl accumulates money to be able to help her medically.', 'inactive', 87, 'X_W1tOngo-w', 'City_Lights', 13),
(17, 'The Exorcist', 'When a teenage girl is possessed by a mysterious entity, her mother seeks the help of two priests to save her daughter.', 'running', 122, 'YDGw1MTEe9k', 'The_Exorcist', 16),
(18, 'Jaws', 'When a gigantic great white shark begins to menace the small island community of Amity, a police chief, a marine scientist and grizzled fisherman set out to stop it.', 'running', 124, 'ucMLFO6TsFM', 'Jaws', 16),
(19, 'A Clockwork Orange', 'In future Britain, charismatic delinquent Alex DeLarge is jailed and volunteers for an experimental aversion therapy developed by the government in an effort to solve society''s crime problem - but not all goes according to plan.', 'upcoming', 136, 'xHFPi_3kc1U', 'A_Clockwork_Orange', 18),
(20, 'Raiders of the Lost Ark', 'Archaeologist and adventurer Indiana Jones is hired by the US government to find the Ark of the Covenant before the Nazis.', 'running', 115, '0ZOcoxjeUYo', 'Raiders_of_the_Lost_Ark', 11),
(21, 'The Maltese Falcon', 'A private detective takes on a case that involves him with three eccentric criminals, a gorgeous liar, and their quest for a priceless statuette.', 'inactive', 100, '7h19sLhvvlg', 'The_Maltese_Falcon', 16),
(22, 'Gladiator', 'When a Roman general is betrayed and his family murdered by an emperor''s corrupt son, he comes to Rome as a gladiator to seek revenge.', 'upcoming', 155, 'AxQajgTyLcM', 'Gladiator', 13),
(23, 'The Lord of the Rings: The Return of the King', 'Gandalf and Aragorn lead the World of Men against Sauron''s army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.', 'inactive', 201, 'r5X-hFf6Bwo', 'TLOTR-TROTK', 11),
(24, 'The Great Dictator', 'Dictator Adenoid Hynkel tries to expand his empire while a poor Jewish barber tries to avoid persecution from Hynkel''s regime.', 'inactive', 125, 'zroWIN-lS8E', 'The_Great_Dictator', 13),
(25, 'Mutiny on the Bounty', 'Fletcher Christian successfully leads a revolt against the ruthless Captain Bligh on the HMS Bounty. However, Bligh returns one year later, hell bent on avenging his captors.', 'inactive', 132, 'MEmZ_A0UTrA', 'Mutiny_on_the_Bounty', 11),
(26, 'Stagecoach', 'A group of people traveling on a stagecoach find their journey complicated by the threat of Geronimo and learn something about each other in the process.', 'inactive', 96, 'BKa0ehuALd0', 'Stagecoach', 11),
(27, 'The African Queen', 'In Africa during WWI, a gin-swilling riverboat captain is persuaded by a strait-laced missionary to use his boat to attack an enemy warship.', 'inactive', 105, '88jytHOsDos', 'The_African_Queen', 13),
(28, 'American Graffiti', 'A couple of high school grads spend one final night cruising the strip with their buddies before they go off to college.', 'inactive', 110, 'HBI0p5OGlDw', 'American_Graffiti', 11),
(29, 'The Graduate', 'A disillusioned college graduate finds himself torn between his older lover and her daughter.', 'inactive', 106, 'hsdvhJTqLak', 'The_Graduate', 13);

-- --------------------------------------------------------

--
-- Table structure for table `seat`
--

CREATE TABLE IF NOT EXISTS `seat` (
`Seat_Id` int(11) NOT NULL,
  `Auditorium` int(11) NOT NULL,
  `Name` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seat`
--

INSERT INTO `seat` (`Seat_Id`, `Auditorium`, `Name`) VALUES
(11, 1, 'A1'),
(12, 1, 'A2'),
(13, 1, 'A3'),
(14, 1, 'A4'),
(15, 1, 'A5'),
(16, 1, 'A6'),
(17, 1, 'A7'),
(18, 1, 'A8'),
(19, 1, 'A9'),
(20, 1, 'A10'),
(21, 1, 'B1'),
(22, 1, 'B2'),
(23, 1, 'B3'),
(24, 1, 'B4'),
(25, 1, 'B5'),
(26, 1, 'B6'),
(27, 1, 'B7'),
(28, 1, 'B8'),
(29, 1, 'B9'),
(30, 1, 'B10'),
(31, 1, 'C1'),
(32, 1, 'C2'),
(33, 1, 'C3'),
(34, 1, 'C4'),
(35, 1, 'C5'),
(36, 1, 'C6'),
(37, 1, 'C7'),
(38, 1, 'C8'),
(39, 1, 'C9'),
(40, 1, 'C10'),
(41, 1, 'D1'),
(42, 1, 'D2'),
(43, 1, 'D3'),
(44, 1, 'D4'),
(45, 1, 'D5'),
(46, 1, 'D6'),
(47, 1, 'D7'),
(48, 1, 'D8'),
(49, 1, 'D9'),
(50, 1, 'D10'),
(51, 1, 'E1'),
(52, 1, 'E2'),
(53, 1, 'E3'),
(54, 1, 'E4'),
(55, 1, 'E5'),
(56, 1, 'E6'),
(57, 1, 'E7'),
(58, 1, 'E8'),
(59, 1, 'E9'),
(60, 1, 'E10'),
(61, 1, 'F1'),
(62, 1, 'F2'),
(63, 1, 'F3'),
(64, 1, 'F4'),
(65, 1, 'F5'),
(66, 1, 'F6'),
(67, 1, 'F7'),
(68, 1, 'F8'),
(69, 1, 'F9'),
(70, 1, 'F10'),
(71, 2, 'A1'),
(72, 2, 'A2'),
(73, 2, 'A3'),
(74, 2, 'A4'),
(75, 2, 'A5'),
(76, 2, 'A6'),
(77, 2, 'A7'),
(78, 2, 'A8'),
(79, 2, 'A9'),
(80, 2, 'A10'),
(81, 2, 'B1'),
(82, 2, 'B2'),
(83, 2, 'B3'),
(84, 2, 'B4'),
(85, 2, 'B5'),
(86, 2, 'B6'),
(87, 2, 'B7'),
(88, 2, 'B8'),
(89, 2, 'B9'),
(90, 2, 'B10'),
(91, 2, 'C1'),
(92, 2, 'C2'),
(93, 2, 'C3'),
(94, 2, 'C4'),
(95, 2, 'C5'),
(96, 2, 'C6'),
(97, 2, 'C7'),
(98, 2, 'C8'),
(99, 2, 'C9'),
(100, 2, 'C10'),
(101, 2, 'D1'),
(102, 2, 'D2'),
(103, 2, 'D3'),
(104, 2, 'D4'),
(105, 2, 'D5'),
(106, 2, 'D6'),
(107, 2, 'D7'),
(108, 2, 'D8'),
(109, 2, 'D9'),
(110, 2, 'D10'),
(111, 2, 'E1'),
(112, 2, 'E2'),
(113, 2, 'E3'),
(114, 2, 'E4'),
(115, 2, 'E5'),
(116, 2, 'E6'),
(117, 2, 'E7'),
(118, 2, 'E8'),
(119, 2, 'E9'),
(120, 2, 'E10'),
(121, 2, 'F1'),
(122, 2, 'F2'),
(123, 2, 'F3'),
(124, 2, 'F4'),
(125, 2, 'F5'),
(126, 2, 'F6'),
(127, 2, 'F7'),
(128, 2, 'F8'),
(129, 2, 'F9'),
(130, 2, 'F10'),
(131, 3, 'A1'),
(132, 3, 'A2'),
(133, 3, 'A3'),
(134, 3, 'A4'),
(135, 3, 'A5'),
(136, 3, 'A6'),
(137, 3, 'A7'),
(138, 3, 'A8'),
(139, 3, 'A9'),
(140, 3, 'A10'),
(141, 3, 'B1'),
(142, 3, 'B2'),
(143, 3, 'B3'),
(144, 3, 'B4'),
(145, 3, 'B5'),
(146, 3, 'B6'),
(147, 3, 'B7'),
(148, 3, 'B8'),
(149, 3, 'B9'),
(150, 3, 'B10'),
(151, 3, 'C1'),
(152, 3, 'C2'),
(153, 3, 'C3'),
(154, 3, 'C4'),
(155, 3, 'C5'),
(156, 3, 'C6'),
(157, 3, 'C7'),
(158, 3, 'C8'),
(159, 3, 'C9'),
(160, 3, 'C10'),
(161, 3, 'D1'),
(162, 3, 'D2'),
(163, 3, 'D3'),
(164, 3, 'D4'),
(165, 3, 'D5'),
(166, 3, 'D6'),
(167, 3, 'D7'),
(168, 3, 'D8'),
(169, 3, 'D9'),
(170, 3, 'D10'),
(171, 3, 'E1'),
(172, 3, 'E2'),
(173, 3, 'E3'),
(174, 3, 'E4'),
(175, 3, 'E5'),
(176, 3, 'E6'),
(177, 3, 'E7'),
(178, 3, 'E8'),
(179, 3, 'E9'),
(180, 3, 'E10'),
(181, 3, 'F1'),
(182, 3, 'F2'),
(183, 3, 'F3'),
(184, 3, 'F4'),
(185, 3, 'F5'),
(186, 3, 'F6'),
(187, 3, 'F7'),
(188, 3, 'F8'),
(189, 3, 'F9'),
(190, 3, 'F10'),
(191, 4, 'A1'),
(192, 4, 'A2'),
(193, 4, 'A3'),
(194, 4, 'A4'),
(195, 4, 'A5'),
(196, 4, 'A6'),
(197, 4, 'A7'),
(198, 4, 'A8'),
(199, 4, 'A9'),
(200, 4, 'A10'),
(201, 4, 'B1'),
(202, 4, 'B2'),
(203, 4, 'B3'),
(204, 4, 'B4'),
(205, 4, 'B5'),
(206, 4, 'B6'),
(207, 4, 'B7'),
(208, 4, 'B8'),
(209, 4, 'B9'),
(210, 4, 'B10'),
(211, 4, 'C1'),
(212, 4, 'C2'),
(213, 4, 'C3'),
(214, 4, 'C4'),
(215, 4, 'C5'),
(216, 4, 'C6'),
(217, 4, 'C7'),
(218, 4, 'C8'),
(219, 4, 'C9'),
(220, 4, 'C10'),
(221, 4, 'D1'),
(222, 4, 'D2'),
(223, 4, 'D3'),
(224, 4, 'D4'),
(225, 4, 'D5'),
(226, 4, 'D6'),
(227, 4, 'D7'),
(228, 4, 'D8'),
(229, 4, 'D9'),
(230, 4, 'D10'),
(231, 4, 'E1'),
(232, 4, 'E2'),
(233, 4, 'E3'),
(234, 4, 'E4'),
(235, 4, 'E5'),
(236, 4, 'E6'),
(237, 4, 'E7'),
(238, 4, 'E8'),
(239, 4, 'E9'),
(240, 4, 'E10'),
(241, 4, 'F1'),
(242, 4, 'F2'),
(243, 4, 'F3'),
(244, 4, 'F4'),
(245, 4, 'F5'),
(246, 4, 'F6'),
(247, 4, 'F7'),
(248, 4, 'F8'),
(249, 4, 'F9'),
(250, 4, 'F10');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
`Ticket_Id` int(11) NOT NULL,
  `Viewing` int(11) NOT NULL,
  `Seat` int(11) NOT NULL,
  `PIN` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `User_Id` varchar(50) NOT NULL,
  `Password` varchar(75) NOT NULL,
  `Role` enum('admin') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `viewing`
--

CREATE TABLE IF NOT EXISTS `viewing` (
`Viewing_Id` int(11) NOT NULL,
  `Movie` int(11) NOT NULL,
  `Auditorium` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auditorium`
--
ALTER TABLE `auditorium`
 ADD PRIMARY KEY (`Auditorium_Id`);

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
 ADD PRIMARY KEY (`Movie_Id`);

--
-- Indexes for table `seat`
--
ALTER TABLE `seat`
 ADD PRIMARY KEY (`Seat_Id`), ADD KEY `audiotorium_id_idx` (`Auditorium`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
 ADD PRIMARY KEY (`Ticket_Id`), ADD KEY `show_id_idx` (`Viewing`), ADD KEY `seat_id_idx` (`Seat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`User_Id`);

--
-- Indexes for table `viewing`
--
ALTER TABLE `viewing`
 ADD PRIMARY KEY (`Viewing_Id`), ADD KEY `movie_id_idx` (`Movie`), ADD KEY `audiotorium_id_idx` (`Auditorium`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auditorium`
--
ALTER TABLE `auditorium`
MODIFY `Auditorium_Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `movie`
--
ALTER TABLE `movie`
MODIFY `Movie_Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `seat`
--
ALTER TABLE `seat`
MODIFY `Seat_Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
MODIFY `Ticket_Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `viewing`
--
ALTER TABLE `viewing`
MODIFY `Viewing_Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `seat`
--
ALTER TABLE `seat`
ADD CONSTRAINT `Aditorium_Id` FOREIGN KEY (`Auditorium`) REFERENCES `auditorium` (`Auditorium_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ticket`
--
ALTER TABLE `ticket`
ADD CONSTRAINT `Seat_Id` FOREIGN KEY (`Seat`) REFERENCES `seat` (`Seat_Id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `Viewing_Id` FOREIGN KEY (`Viewing`) REFERENCES `viewing` (`Viewing_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `viewing`
--
ALTER TABLE `viewing`
ADD CONSTRAINT `Auditorium_Id` FOREIGN KEY (`Auditorium`) REFERENCES `auditorium` (`Auditorium_Id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `Movie_Id` FOREIGN KEY (`Movie`) REFERENCES `movie` (`Movie_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
